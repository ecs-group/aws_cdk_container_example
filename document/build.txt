C:\Users\John\dev\repo\ecs-group\da-blogs\aws_cdk_container_example>cdk deploy
This deployment will make potentially sensitive changes according to your current security approval level (--require-approval broadening).
Please confirm you intend to make the following modifications:

IAM Statement Changes
┌───┬────────────────────────────────────────────────┬────────┬───────────────────────────────────────┬────────────────────────────────────────────────┬───────────┐
│   │ Resource                                       │ Effect │ Action                                │ Principal                                      │ Condition │
├───┼────────────────────────────────────────────────┼────────┼───────────────────────────────────────┼────────────────────────────────────────────────┼───────────┤
│ + │ ${EcsFargateService/TaskDef/ExecutionRole.Arn} │ Allow  │ sts:AssumeRole                        │ Service:ecs-tasks.amazonaws.com                │           │
├───┼────────────────────────────────────────────────┼────────┼───────────────────────────────────────┼────────────────────────────────────────────────┼───────────┤
│ + │ ${EcsFargateService/TaskDef/TaskRole.Arn}      │ Allow  │ sts:AssumeRole                        │ Service:ecs-tasks.amazonaws.com                │           │
├───┼────────────────────────────────────────────────┼────────┼───────────────────────────────────────┼────────────────────────────────────────────────┼───────────┤
│ + │ ${EcsFargateService/TaskDef/web/LogGroup.Arn}  │ Allow  │ logs:CreateLogStream                  │ AWS:${EcsFargateService/TaskDef/ExecutionRole} │           │
│   │                                                │        │ logs:PutLogEvents                     │                                                │           │
└───┴────────────────────────────────────────────────┴────────┴───────────────────────────────────────┴────────────────────────────────────────────────┴───────────┘
Security Group Changes
┌───┬────────────────────────────────────────────────────┬─────┬────────────┬────────────────────────────────────────────────────┐
│   │ Group                                              │ Dir │ Protocol   │ Peer                                               │
├───┼────────────────────────────────────────────────────┼─────┼────────────┼────────────────────────────────────────────────────┤
│ + │ ${EcsFargateService/LB/SecurityGroup.GroupId}      │ In  │ TCP 80     │ Everyone (IPv4)                                    │
│ + │ ${EcsFargateService/LB/SecurityGroup.GroupId}      │ Out │ TCP 80     │ ${EcsFargateService/Service/SecurityGroup.GroupId} │
├───┼────────────────────────────────────────────────────┼─────┼────────────┼────────────────────────────────────────────────────┤
│ + │ ${EcsFargateService/Service/SecurityGroup.GroupId} │ In  │ TCP 80     │ ${EcsFargateService/LB/SecurityGroup.GroupId}      │
│ + │ ${EcsFargateService/Service/SecurityGroup.GroupId} │ Out │ Everything │ Everyone (IPv4)                                    │
└───┴────────────────────────────────────────────────────┴─────┴────────────┴────────────────────────────────────────────────────┘
(NOTE: There may be security-related changes not in this list. See https://github.com/aws/aws-cdk/issues/1299)

Do you wish to deploy these changes (y/n)? y
EcsCdkFargateStack: deploying...
EcsCdkFargateStack: creating CloudFormation changeset...
  0/39 | 15:06:44 | CREATE_IN_PROGRESS   | AWS::CloudFormation::Stack                | EcsCdkFargateStack User Initiated
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::CDK::Metadata                        | CDKMetadata
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::EC2::InternetGateway                 | EcsVpc/IGW (EcsVpcIGWF80402C4)
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::ECS::Cluster                         | EcsCluster (EcsCluster97242B84)
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::IAM::Role                            | EcsFargateService/TaskDef/TaskRole (EcsFargateServiceTaskDefTaskRole47FB3082)
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::EC2::EIP                             | EcsVpc/PublicSubnet2/EIP (EcsVpcPublicSubnet2EIP9B6C83AE)
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::EC2::EIP                             | EcsVpc/PublicSubnet1/EIP (EcsVpcPublicSubnet1EIP97CC60E8)
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::Logs::LogGroup                       | EcsFargateService/TaskDef/web/LogGroup (EcsFargateServiceTaskDefwebLogGroup12DEDA1C)
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::IAM::Role                            | EcsFargateService/TaskDef/ExecutionRole (EcsFargateServiceTaskDefExecutionRole5AF2B8AE)
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::EC2::VPC                             | EcsVpc (EcsVpcCEF3634B)
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::EC2::InternetGateway                 | EcsVpc/IGW (EcsVpcIGWF80402C4) Resource creation Initiated
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::IAM::Role                            | EcsFargateService/TaskDef/TaskRole (EcsFargateServiceTaskDefTaskRole47FB3082) Resource creation Initiated
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::Logs::LogGroup                       | EcsFargateService/TaskDef/web/LogGroup (EcsFargateServiceTaskDefwebLogGroup12DEDA1C) Resource creation Initiated
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::EC2::EIP                             | EcsVpc/PublicSubnet2/EIP (EcsVpcPublicSubnet2EIP9B6C83AE) Resource creation Initiated
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::EC2::EIP                             | EcsVpc/PublicSubnet1/EIP (EcsVpcPublicSubnet1EIP97CC60E8) Resource creation Initiated
  0/39 | 15:06:48 | CREATE_IN_PROGRESS   | AWS::EC2::VPC                             | EcsVpc (EcsVpcCEF3634B) Resource creation Initiated
  1/39 | 15:06:49 | CREATE_COMPLETE      | AWS::Logs::LogGroup                       | EcsFargateService/TaskDef/web/LogGroup (EcsFargateServiceTaskDefwebLogGroup12DEDA1C)
  1/39 | 15:06:49 | CREATE_IN_PROGRESS   | AWS::IAM::Role                            | EcsFargateService/TaskDef/ExecutionRole (EcsFargateServiceTaskDefExecutionRole5AF2B8AE) Resource creation Initiated
  1/39 | 15:06:49 | CREATE_IN_PROGRESS   | AWS::CDK::Metadata                        | CDKMetadata Resource creation Initiated
  2/39 | 15:06:49 | CREATE_COMPLETE      | AWS::CDK::Metadata                        | CDKMetadata
  2/39 | 15:06:50 | CREATE_IN_PROGRESS   | AWS::ECS::Cluster                         | EcsCluster (EcsCluster97242B84) Resource creation Initiated
  3/39 | 15:06:54 | CREATE_COMPLETE      | AWS::ECS::Cluster                         | EcsCluster (EcsCluster97242B84)
  4/39 | 15:07:04 | CREATE_COMPLETE      | AWS::EC2::InternetGateway                 | EcsVpc/IGW (EcsVpcIGWF80402C4)
  5/39 | 15:07:04 | CREATE_COMPLETE      | AWS::EC2::EIP                             | EcsVpc/PublicSubnet2/EIP (EcsVpcPublicSubnet2EIP9B6C83AE)
  6/39 | 15:07:04 | CREATE_COMPLETE      | AWS::EC2::EIP                             | EcsVpc/PublicSubnet1/EIP (EcsVpcPublicSubnet1EIP97CC60E8)
  7/39 | 15:07:05 | CREATE_COMPLETE      | AWS::EC2::VPC                             | EcsVpc (EcsVpcCEF3634B)
  8/39 | 15:07:05 | CREATE_COMPLETE      | AWS::IAM::Role                            | EcsFargateService/TaskDef/TaskRole (EcsFargateServiceTaskDefTaskRole47FB3082)
  9/39 | 15:07:06 | CREATE_COMPLETE      | AWS::IAM::Role                            | EcsFargateService/TaskDef/ExecutionRole (EcsFargateServiceTaskDefExecutionRole5AF2B8AE)
  9/39 | 15:07:06 | CREATE_IN_PROGRESS   | AWS::EC2::RouteTable                      | EcsVpc/PrivateSubnet1/RouteTable (EcsVpcPrivateSubnet1RouteTable44D603B2)
  9/39 | 15:07:06 | CREATE_IN_PROGRESS   | AWS::EC2::RouteTable                      | EcsVpc/PublicSubnet1/RouteTable (EcsVpcPublicSubnet1RouteTable3F836709)
  9/39 | 15:07:06 | CREATE_IN_PROGRESS   | AWS::EC2::SecurityGroup                   | EcsFargateService/LB/SecurityGroup (EcsFargateServiceLBSecurityGroup08B4D383)
  9/39 | 15:07:06 | CREATE_IN_PROGRESS   | AWS::EC2::RouteTable                      | EcsVpc/PrivateSubnet2/RouteTable (EcsVpcPrivateSubnet2RouteTable1CB3B77F)
  9/39 | 15:07:06 | CREATE_IN_PROGRESS   | AWS::EC2::SecurityGroup                   | EcsFargateService/Service/SecurityGroup (EcsFargateServiceSecurityGroupAC6355C6)
  9/39 | 15:07:06 | CREATE_IN_PROGRESS   | AWS::EC2::RouteTable                      | EcsVpc/PrivateSubnet1/RouteTable (EcsVpcPrivateSubnet1RouteTable44D603B2) Resource creation Initiated
  9/39 | 15:07:06 | CREATE_IN_PROGRESS   | AWS::EC2::RouteTable                      | EcsVpc/PublicSubnet2/RouteTable (EcsVpcPublicSubnet2RouteTable259A8B2D)
  9/39 | 15:07:06 | CREATE_IN_PROGRESS   | AWS::EC2::Subnet                          | EcsVpc/PublicSubnet2/Subnet (EcsVpcPublicSubnet2Subnet127B1F4A)
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::VPCGatewayAttachment            | EcsVpc/VPCGW (EcsVpcVPCGWE2526015)
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::Subnet                          | EcsVpc/PrivateSubnet1/Subnet (EcsVpcPrivateSubnet1SubnetDE1C6717)
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::ElasticLoadBalancingV2::TargetGroup  | EcsFargateService/LB/PublicListener/ECSGroup (EcsFargateServiceLBPublicListenerECSGroup21A1FFA5)
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::Subnet                          | EcsVpc/PrivateSubnet2/Subnet (EcsVpcPrivateSubnet2Subnet4E5EDAB1)
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::RouteTable                      | EcsVpc/PublicSubnet1/RouteTable (EcsVpcPublicSubnet1RouteTable3F836709) Resource creation Initiated
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::Subnet                          | EcsVpc/PublicSubnet1/Subnet (EcsVpcPublicSubnet1Subnet4FC26CFD)
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::RouteTable                      | EcsVpc/PrivateSubnet2/RouteTable (EcsVpcPrivateSubnet2RouteTable1CB3B77F) Resource creation Initiated
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::Subnet                          | EcsVpc/PublicSubnet2/Subnet (EcsVpcPublicSubnet2Subnet127B1F4A) Resource creation Initiated
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::VPCGatewayAttachment            | EcsVpc/VPCGW (EcsVpcVPCGWE2526015) Resource creation Initiated
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::RouteTable                      | EcsVpc/PublicSubnet2/RouteTable (EcsVpcPublicSubnet2RouteTable259A8B2D) Resource creation Initiated
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::ElasticLoadBalancingV2::TargetGroup  | EcsFargateService/LB/PublicListener/ECSGroup (EcsFargateServiceLBPublicListenerECSGroup21A1FFA5) Resource creation Initiated
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::Subnet                          | EcsVpc/PrivateSubnet2/Subnet (EcsVpcPrivateSubnet2Subnet4E5EDAB1) Resource creation Initiated
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::Subnet                          | EcsVpc/PrivateSubnet1/Subnet (EcsVpcPrivateSubnet1SubnetDE1C6717) Resource creation Initiated
  9/39 | 15:07:07 | CREATE_IN_PROGRESS   | AWS::EC2::Subnet                          | EcsVpc/PublicSubnet1/Subnet (EcsVpcPublicSubnet1Subnet4FC26CFD) Resource creation Initiated
 10/39 | 15:07:07 | CREATE_COMPLETE      | AWS::ElasticLoadBalancingV2::TargetGroup  | EcsFargateService/LB/PublicListener/ECSGroup (EcsFargateServiceLBPublicListenerECSGroup21A1FFA5)
 11/39 | 15:07:07 | CREATE_COMPLETE      | AWS::EC2::RouteTable                      | EcsVpc/PrivateSubnet1/RouteTable (EcsVpcPrivateSubnet1RouteTable44D603B2)
 12/39 | 15:07:07 | CREATE_COMPLETE      | AWS::EC2::RouteTable                      | EcsVpc/PublicSubnet1/RouteTable (EcsVpcPublicSubnet1RouteTable3F836709)
 13/39 | 15:07:08 | CREATE_COMPLETE      | AWS::EC2::RouteTable                      | EcsVpc/PrivateSubnet2/RouteTable (EcsVpcPrivateSubnet2RouteTable1CB3B77F)
 14/39 | 15:07:08 | CREATE_COMPLETE      | AWS::EC2::RouteTable                      | EcsVpc/PublicSubnet2/RouteTable (EcsVpcPublicSubnet2RouteTable259A8B2D)
 14/39 | 15:07:08 | CREATE_IN_PROGRESS   | AWS::IAM::Policy                          | EcsFargateService/TaskDef/ExecutionRole/DefaultPolicy (EcsFargateServiceTaskDefExecutionRoleDefaultPolicy4D1C809D)
 14/39 | 15:07:08 | CREATE_IN_PROGRESS   | AWS::ECS::TaskDefinition                  | EcsFargateService/TaskDef (EcsFargateServiceTaskDef21E57BC2)
 14/39 | 15:07:08 | CREATE_IN_PROGRESS   | AWS::ECS::TaskDefinition                  | EcsFargateService/TaskDef (EcsFargateServiceTaskDef21E57BC2) Resource creation Initiated
 15/39 | 15:07:09 | CREATE_COMPLETE      | AWS::ECS::TaskDefinition                  | EcsFargateService/TaskDef (EcsFargateServiceTaskDef21E57BC2)
 15/39 | 15:07:09 | CREATE_IN_PROGRESS   | AWS::IAM::Policy                          | EcsFargateService/TaskDef/ExecutionRole/DefaultPolicy (EcsFargateServiceTaskDefExecutionRoleDefaultPolicy4D1C809D) Resource creation Initiated
 15/39 | 15:07:11 | CREATE_IN_PROGRESS   | AWS::EC2::SecurityGroup                   | EcsFargateService/LB/SecurityGroup (EcsFargateServiceLBSecurityGroup08B4D383) Resource creation Initiated
 15/39 | 15:07:11 | CREATE_IN_PROGRESS   | AWS::EC2::SecurityGroup                   | EcsFargateService/Service/SecurityGroup (EcsFargateServiceSecurityGroupAC6355C6) Resource creation Initiated
 16/39 | 15:07:12 | CREATE_COMPLETE      | AWS::EC2::SecurityGroup                   | EcsFargateService/LB/SecurityGroup (EcsFargateServiceLBSecurityGroup08B4D383)
 17/39 | 15:07:12 | CREATE_COMPLETE      | AWS::EC2::SecurityGroup                   | EcsFargateService/Service/SecurityGroup (EcsFargateServiceSecurityGroupAC6355C6)
 17/39 | 15:07:14 | CREATE_IN_PROGRESS   | AWS::EC2::SecurityGroupEgress             | EcsFargateService/LB/SecurityGroup/to EcsCdkFargateStackEcsFargateServiceSecurityGroupDCD725D0:80 (EcsFargateServiceLBSecurityGrouptoEcsCdkFargateStackEcsFargateServiceSecurityGroupDCD725D080C5638EBF)
 17/39 | 15:07:14 | CREATE_IN_PROGRESS   | AWS::EC2::SecurityGroupIngress            | EcsFargateService/Service/SecurityGroup/from EcsCdkFargateStackEcsFargateServiceLBSecurityGroupE44E3892:80 (EcsFargateServiceSecurityGroupfromEcsCdkFargateStackEcsFargateServiceLBSecurityGroupE44E389280CDBD55C9)
 17/39 | 15:07:14 | CREATE_IN_PROGRESS   | AWS::EC2::SecurityGroupIngress            | EcsFargateService/Service/SecurityGroup/from EcsCdkFargateStackEcsFargateServiceLBSecurityGroupE44E3892:80 (EcsFargateServiceSecurityGroupfromEcsCdkFargateStackEcsFargateServiceLBSecurityGroupE44E389280CDBD55C9) Resource creation Initiated
 17/39 | 15:07:14 | CREATE_IN_PROGRESS   | AWS::EC2::SecurityGroupEgress             | EcsFargateService/LB/SecurityGroup/to EcsCdkFargateStackEcsFargateServiceSecurityGroupDCD725D0:80 (EcsFargateServiceLBSecurityGrouptoEcsCdkFargateStackEcsFargateServiceSecurityGroupDCD725D080C5638EBF) Resource creation Initiated
 18/39 | 15:07:15 | CREATE_COMPLETE      | AWS::EC2::SecurityGroupIngress            | EcsFargateService/Service/SecurityGroup/from EcsCdkFargateStackEcsFargateServiceLBSecurityGroupE44E3892:80 (EcsFargateServiceSecurityGroupfromEcsCdkFargateStackEcsFargateServiceLBSecurityGroupE44E389280CDBD55C9)
 19/39 | 15:07:15 | CREATE_COMPLETE      | AWS::EC2::SecurityGroupEgress             | EcsFargateService/LB/SecurityGroup/to EcsCdkFargateStackEcsFargateServiceSecurityGroupDCD725D0:80 (EcsFargateServiceLBSecurityGrouptoEcsCdkFargateStackEcsFargateServiceSecurityGroupDCD725D080C5638EBF)
 20/39 | 15:07:22 | CREATE_COMPLETE      | AWS::EC2::VPCGatewayAttachment            | EcsVpc/VPCGW (EcsVpcVPCGWE2526015)
 21/39 | 15:07:23 | CREATE_COMPLETE      | AWS::EC2::Subnet                          | EcsVpc/PublicSubnet2/Subnet (EcsVpcPublicSubnet2Subnet127B1F4A)
 22/39 | 15:07:23 | CREATE_COMPLETE      | AWS::EC2::Subnet                          | EcsVpc/PrivateSubnet1/Subnet (EcsVpcPrivateSubnet1SubnetDE1C6717)
 23/39 | 15:07:23 | CREATE_COMPLETE      | AWS::EC2::Subnet                          | EcsVpc/PrivateSubnet2/Subnet (EcsVpcPrivateSubnet2Subnet4E5EDAB1)
 24/39 | 15:07:23 | CREATE_COMPLETE      | AWS::EC2::Subnet                          | EcsVpc/PublicSubnet1/Subnet (EcsVpcPublicSubnet1Subnet4FC26CFD)
 24/39 | 15:07:24 | CREATE_IN_PROGRESS   | AWS::EC2::Route                           | EcsVpc/PublicSubnet1/DefaultRoute (EcsVpcPublicSubnet1DefaultRoute29B4D851)
 24/39 | 15:07:24 | CREATE_IN_PROGRESS   | AWS::EC2::Route                           | EcsVpc/PublicSubnet2/DefaultRoute (EcsVpcPublicSubnet2DefaultRouteD785541D)
 24/39 | 15:07:24 | CREATE_IN_PROGRESS   | AWS::EC2::Route                           | EcsVpc/PublicSubnet1/DefaultRoute (EcsVpcPublicSubnet1DefaultRoute29B4D851) Resource creation Initiated
 24/39 | 15:07:24 | CREATE_IN_PROGRESS   | AWS::EC2::Route                           | EcsVpc/PublicSubnet2/DefaultRoute (EcsVpcPublicSubnet2DefaultRouteD785541D) Resource creation Initiated
 24/39 | 15:07:24 | CREATE_IN_PROGRESS   | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PrivateSubnet1/RouteTableAssociation (EcsVpcPrivateSubnet1RouteTableAssociationA072EBC4)
 24/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PublicSubnet2/RouteTableAssociation (EcsVpcPublicSubnet2RouteTableAssociation6449DB9B)
 24/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::NatGateway                      | EcsVpc/PublicSubnet1/NATGateway (EcsVpcPublicSubnet1NATGateway84F4640B)
 24/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::NatGateway                      | EcsVpc/PublicSubnet2/NATGateway (EcsVpcPublicSubnet2NATGatewayDFAD96D0)
 24/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PrivateSubnet2/RouteTableAssociation (EcsVpcPrivateSubnet2RouteTableAssociation136FEFD9)
 24/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PublicSubnet1/RouteTableAssociation (EcsVpcPublicSubnet1RouteTableAssociation27B0B1F1)
 24/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::NatGateway                      | EcsVpc/PublicSubnet1/NATGateway (EcsVpcPublicSubnet1NATGateway84F4640B) Resource creation Initiated
 24/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::NatGateway                      | EcsVpc/PublicSubnet2/NATGateway (EcsVpcPublicSubnet2NATGatewayDFAD96D0) Resource creation Initiated
 25/39 | 15:07:25 | CREATE_COMPLETE      | AWS::IAM::Policy                          | EcsFargateService/TaskDef/ExecutionRole/DefaultPolicy (EcsFargateServiceTaskDefExecutionRoleDefaultPolicy4D1C809D)
 25/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PublicSubnet2/RouteTableAssociation (EcsVpcPublicSubnet2RouteTableAssociation6449DB9B) Resource creation Initiated
 25/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PrivateSubnet1/RouteTableAssociation (EcsVpcPrivateSubnet1RouteTableAssociationA072EBC4) Resource creation Initiated
 25/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PrivateSubnet2/RouteTableAssociation (EcsVpcPrivateSubnet2RouteTableAssociation136FEFD9) Resource creation Initiated
 25/39 | 15:07:25 | CREATE_IN_PROGRESS   | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PublicSubnet1/RouteTableAssociation (EcsVpcPublicSubnet1RouteTableAssociation27B0B1F1) Resource creation Initiated
 26/39 | 15:07:39 | CREATE_COMPLETE      | AWS::EC2::Route                           | EcsVpc/PublicSubnet1/DefaultRoute (EcsVpcPublicSubnet1DefaultRoute29B4D851)
 27/39 | 15:07:39 | CREATE_COMPLETE      | AWS::EC2::Route                           | EcsVpc/PublicSubnet2/DefaultRoute (EcsVpcPublicSubnet2DefaultRouteD785541D)
 28/39 | 15:07:41 | CREATE_COMPLETE      | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PublicSubnet2/RouteTableAssociation (EcsVpcPublicSubnet2RouteTableAssociation6449DB9B)
 29/39 | 15:07:41 | CREATE_COMPLETE      | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PrivateSubnet1/RouteTableAssociation (EcsVpcPrivateSubnet1RouteTableAssociationA072EBC4)
 30/39 | 15:07:41 | CREATE_COMPLETE      | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PrivateSubnet2/RouteTableAssociation (EcsVpcPrivateSubnet2RouteTableAssociation136FEFD9)
 31/39 | 15:07:41 | CREATE_COMPLETE      | AWS::EC2::SubnetRouteTableAssociation     | EcsVpc/PublicSubnet1/RouteTableAssociation (EcsVpcPublicSubnet1RouteTableAssociation27B0B1F1)
 31/39 | 15:07:41 | CREATE_IN_PROGRESS   | AWS::ElasticLoadBalancingV2::LoadBalancer | EcsFargateService/LB (EcsFargateServiceLB62BD16B4)
 31/39 | 15:07:42 | CREATE_IN_PROGRESS   | AWS::ElasticLoadBalancingV2::LoadBalancer | EcsFargateService/LB (EcsFargateServiceLB62BD16B4) Resource creation Initiated
31/39 Currently in progress: EcsCdkFargateStack, EcsVpcPublicSubnet1NATGateway84F4640B, EcsVpcPublicSubnet2NATGatewayDFAD96D0, EcsFargateServiceLB62BD16B4
 32/39 | 15:08:57 | CREATE_COMPLETE      | AWS::EC2::NatGateway                      | EcsVpc/PublicSubnet1/NATGateway (EcsVpcPublicSubnet1NATGateway84F4640B)
 32/39 | 15:08:59 | CREATE_IN_PROGRESS   | AWS::EC2::Route                           | EcsVpc/PrivateSubnet1/DefaultRoute (EcsVpcPrivateSubnet1DefaultRoute523B7F1A)
 32/39 | 15:09:00 | CREATE_IN_PROGRESS   | AWS::EC2::Route                           | EcsVpc/PrivateSubnet1/DefaultRoute (EcsVpcPrivateSubnet1DefaultRoute523B7F1A) Resource creation Initiated
 33/39 | 15:09:15 | CREATE_COMPLETE      | AWS::EC2::Route                           | EcsVpc/PrivateSubnet1/DefaultRoute (EcsVpcPrivateSubnet1DefaultRoute523B7F1A)
 34/39 | 15:09:28 | CREATE_COMPLETE      | AWS::EC2::NatGateway                      | EcsVpc/PublicSubnet2/NATGateway (EcsVpcPublicSubnet2NATGatewayDFAD96D0)
 34/39 | 15:09:30 | CREATE_IN_PROGRESS   | AWS::EC2::Route                           | EcsVpc/PrivateSubnet2/DefaultRoute (EcsVpcPrivateSubnet2DefaultRoute1D92C4ED)
 34/39 | 15:09:30 | CREATE_IN_PROGRESS   | AWS::EC2::Route                           | EcsVpc/PrivateSubnet2/DefaultRoute (EcsVpcPrivateSubnet2DefaultRoute1D92C4ED) Resource creation Initiated
 35/39 | 15:09:45 | CREATE_COMPLETE      | AWS::EC2::Route                           | EcsVpc/PrivateSubnet2/DefaultRoute (EcsVpcPrivateSubnet2DefaultRoute1D92C4ED)
 36/39 | 15:10:13 | CREATE_COMPLETE      | AWS::ElasticLoadBalancingV2::LoadBalancer | EcsFargateService/LB (EcsFargateServiceLB62BD16B4)
 36/39 | 15:10:15 | CREATE_IN_PROGRESS   | AWS::ElasticLoadBalancingV2::Listener     | EcsFargateService/LB/PublicListener (EcsFargateServiceLBPublicListener58805FE0)
 36/39 | 15:10:15 | CREATE_IN_PROGRESS   | AWS::ElasticLoadBalancingV2::Listener     | EcsFargateService/LB/PublicListener (EcsFargateServiceLBPublicListener58805FE0) Resource creation Initiated
 37/39 | 15:10:15 | CREATE_COMPLETE      | AWS::ElasticLoadBalancingV2::Listener     | EcsFargateService/LB/PublicListener (EcsFargateServiceLBPublicListener58805FE0)
 37/39 | 15:10:17 | CREATE_IN_PROGRESS   | AWS::ECS::Service                         | EcsFargateService/Service/Service (EcsFargateService400C7E52)
 37/39 | 15:10:18 | CREATE_IN_PROGRESS   | AWS::ECS::Service                         | EcsFargateService/Service/Service (EcsFargateService400C7E52) Resource creation Initiated
37/39 Currently in progress: EcsCdkFargateStack, EcsFargateService400C7E52

 ✅  EcsCdkFargateStack

Outputs:
EcsCdkFargateStack.EcsFargateServiceLoadBalancerDNSBC54228A = EcsCd-EcsFa-ADRKP53IUKXS-66698077.eu-west-2.elb.amazonaws.com
EcsCdkFargateStack.EcsFargateServiceServiceURLA6FBAC44 = http://EcsCd-EcsFa-ADRKP53IUKXS-66698077.eu-west-2.elb.amazonaws.com

Stack ARN:
arn:aws:cloudformation:eu-west-2:149121528372:stack/EcsCdkFargateStack/e9ea8880-bada-11ea-b086-0a7f7081acf8