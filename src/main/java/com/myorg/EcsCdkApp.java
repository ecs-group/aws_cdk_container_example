package com.myorg;

import software.amazon.awscdk.core.App;

public class EcsCdkApp {
    public static void main(final String[] args) {
        App app = new App();

        new EcsCdkFargateStack(app, "EcsCdkFargateStack");

        app.synth();
    }
}
