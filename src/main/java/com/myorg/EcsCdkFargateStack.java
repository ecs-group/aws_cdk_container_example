package com.myorg;

import software.amazon.awscdk.core.Construct;
import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.core.StackProps;
import software.amazon.awscdk.services.ec2.Vpc;
import software.amazon.awscdk.services.ecs.Cluster;
import software.amazon.awscdk.services.ecs.ContainerImage;
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedFargateService;
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedTaskImageOptions;

public class EcsCdkFargateStack extends Stack {

    private static final String DOCKERHUB_CONTAINER_NAME = "johndobie/ecs-spring-boot-echo:latest";

    private static final String PREFIX = "Ecs";
    private static final String VPC_NAME = PREFIX + "Vpc";
    private static final String CLUSTER_NAME = PREFIX + "Cluster";

    private static final String FARGATE_SERVICE_NAME = PREFIX + "FargateService";

    private static final int FARGATE_LISTENER_PORT = 80;
    private static final int MEMORY_LIMIT_MI_B = 1024;
    private static final int DESIRED_CLUSTER_COUNT = 1;
    private static final int CPU_SIZE = 256;
    private static final int MAX_AZS = 2;
    private static final boolean PUBLIC_LOAD_BALANCER = true;

    public EcsCdkFargateStack(final Construct scope, final String id) {
        this(scope, id, null);
    }

    private EcsCdkFargateStack(final Construct scope, final String id, final StackProps props) {
        super(scope, id, props);

        Vpc vpc = Vpc.Builder
                          .create(this, VPC_NAME)
                          .maxAzs(MAX_AZS)  // Default is all AZs in region
                          .build();

        Cluster cluster = Cluster.Builder
                                  .create(this, CLUSTER_NAME)
                                  .vpc(vpc)
                                  .build();

        // Create a load-balanced Fargate service and make it public
        ApplicationLoadBalancedFargateService.Builder
                .create(this, FARGATE_SERVICE_NAME)
                .cluster(cluster)
                .cpu(CPU_SIZE)
                .desiredCount(DESIRED_CLUSTER_COUNT)
                .taskImageOptions(
                        ApplicationLoadBalancedTaskImageOptions
                                .builder()
                                .image(ContainerImage.fromRegistry(DOCKERHUB_CONTAINER_NAME))
                                .build())
                .memoryLimitMiB(MEMORY_LIMIT_MI_B)
                .publicLoadBalancer(PUBLIC_LOAD_BALANCER)
                .listenerPort(FARGATE_LISTENER_PORT)
                .build();
    }
}
